from graphene_django import DjangoObjectType
from products.models import Product,View,Category
from users.models import CustomUser


class CategoryType(DjangoObjectType):
    """Objecto devuelto es Category junto con todos sus atributos."""
    class Meta:
        model = Category
        fields = ("__all__")


class ProductType(DjangoObjectType):
    """Objecto devuelto es Product junto con todos sus atributos."""
    class Meta:
        model = Product
        fields = ("__all__")


class ViewType(DjangoObjectType):
    """Objecto devuelto es Product junto con todos sus atributos."""
    class Meta:
        model = View
        fields = ("__all__")

class UserType(DjangoObjectType):
    """Objecto devuelto es User junto con todos sus atributos."""
    class Meta:
        model = CustomUser
        fields = ("__all__")