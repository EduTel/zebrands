FROM python:3.9.1-alpine3.13
WORKDIR /app
COPY ./ZeBrands .
COPY ./ZeBrands .
COPY ./manage.py .
EXPOSE 8000/tcp
RUN pip3  --no-cache-dir --use-feature=2020-resolver install -r requirements.txt  --no-deps
CMD ["python3", "./manage.py", "runserver", "0.0.0.0:8000"]
# run gunicorn
#CMD gunicorn ZeBrands.wsgi:application --bind 0.0.0.0:$PORT
