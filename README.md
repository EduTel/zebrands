# :star:ZeBrands:star:

## Requerimientos
As a special requirement, whenever an admin user makes a change in a product (for example, if a price is adjusted), we need to notify all other admins about the change, either via email or other mechanism.

We also need to keep track of the number of times every single product is queried by an anonymous user, so we can build some reports in the future.

## aws ses
* ***ejecutar codigo localmente***
```
export SENDER="your email <youemail@example.com>" && python manage.py runserver
```

* ***Proporcionar credenciales y configuración de aws para docker***
      recuerde no estar en un entorno de pruebas
```
-e SENDER='your email <youemail@example.com>'

docker run --rm -it -v ~/.aws:/root/.aws amazon/aws-cli
```

# importante
Para la facilidad de probar la aplicacion se adjunta el arcivo **graphql.gql** 

# db
* Product (obj_products)

| sku | name | price | obj_categoria | created_at | updated_at |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |

* Category (obj_categoria)

| str_nombre | created_at | updated_at |
| ----------- | ----------- | ----------- |

* View

| obj_products | str_times | created_at | updated_at |
| ----------- | ----------- | ----------- | ----------- |

(i) admins to create / update / delete products and to create / update / delete other admins; and 
(ii) anonymous users who can only retrieve products information but can't make changes.

## Notas
la aplicacion viene con unos campos precargados, en caso de querer borrar la bd **db.sqlite3** en la aplicacion hay una carpeta llamada **load_data** aqui se encuentran unos datos de prueba **\*.json** como referencia para empezar a ocupar la aplicacion

## Para volver a generar la documentacion
      1 pycco products/models.py -p
      2 pycco products/schema.py -p
      3 pycco <path>/<file> -p

## Manera de mandar el token
* se optiene un **Token** con las funciones (register y tokenAuth) si no se envia un **Token** como header significa que es un ususario anonimo

**EJEMPLO**
```json
{
      "Authorization": "JWT \<Token>"
}
```

## url relacionadas
* url gitlab
https://gitlab.com/EduTel/zebrands
* url del despliegue
https://edutel-zebrands.herokuapp.com/graphql
* url de docker hub
https://hub.docker.com/r/edutel/zebrands
docker push edutel/zebrands:tagname

# Escalabilidad 

* cambiar la db por una como postgresql o mongodb 
* agregar roles a los usuarios
* validar correos al crearse un usuario 



